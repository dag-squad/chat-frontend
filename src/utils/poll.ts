interface Poll<T> {
  fn: () => Promise<Response>;
  validate: (res: Response, json: T) => boolean;
  interval?: number;
  maxAttempts?: number;
}

async function poll<T>({
  fn,
  validate,
  interval = 1000,
  maxAttempts,
}: Poll<T>): Promise<T> {
  let attempts = 0;

  const runPoll = async (
    resolve: (json: T) => void,
    reject: (err: Error) => void,
  ) => {
    const result = await fn();
    const json = await result.json();
    attempts += 1;

    if (validate(result, json)) {
      return resolve(json);
    }
    if (maxAttempts && attempts === maxAttempts) {
      return reject(new Error('Exceeded max attempts'));
    }
    setTimeout(runPoll, interval, resolve, reject);
    return null;
  };

  return new Promise(runPoll);
}

export default poll;
