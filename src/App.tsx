import React from 'react';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import './App.scss';
import { Answer, Offer } from '@pages';
import { PeerConnectionProvider } from '@contexts';

const App = (): JSX.Element => (
  <Router>
    <Switch>
      <Route exact path="/">
        <Offer />
      </Route>
      <Route path="/room/:id">
        <PeerConnectionProvider>
          <Answer />
        </PeerConnectionProvider>
      </Route>
    </Switch>
  </Router>
);

export default App;
