import React, { createContext, useContext, useEffect, useRef, useState } from 'react';

import { useParams } from 'react-router-dom';

import config from '@/config';
import poll from '@utils';

interface Context {
  createOffer(): Promise<void>;
  createAnswer(offerDescription: RTCSessionDescriptionInit): Promise<void>;
  setLocalStream(stream: MediaStream): void;
  getRemoteStream(): Promise<MediaStream>;
  hangup(): void;
}

type PeerConnectionProviderProps = { children: React.ReactNode };

const PeerConnectionContext = createContext<Context | undefined>(undefined);

export const PeerConnectionProvider = ({ children }: PeerConnectionProviderProps): JSX.Element => {
  const [isPollAnswer, setIsPollAnswer] = useState(false);
  const [isPollOfferCandidates, setIsPollOfferCandidates] = useState(false);
  const [isPollAnswerCandidates, setIsPollAnswerCandidates] = useState(false);
  const pc = useRef<RTCPeerConnection | null>(null);
  const { id } = useParams<{ id: string }>();

  const hangup = (): void => {
    pc.current?.close();
    pc.current = null;
  };

  useEffect(() => {
    pc.current = new RTCPeerConnection();

    return hangup;
  }, []);

  useEffect(() => {
    if (isPollAnswer) {
      poll<RTCSessionDescriptionInit>({
        fn: () => fetch(`${config.API_URL}/rooms/${id}/answer`),
        validate: (res) => res.status === 200,
      })
        .then((answer) => {
          if (!pc.current?.currentRemoteDescription && answer) {
            const answerDescription = new RTCSessionDescription(answer);
            pc.current?.setRemoteDescription(answerDescription);
          }
          return null;
        })
        .catch(console.error);
    }
  }, [isPollAnswer]);

  useEffect(() => {
    if (isPollOfferCandidates) {
      poll<{ candidates: RTCIceCandidate[] }>({
        fn: () => fetch(`${config.API_URL}/rooms/${id}/offer-candidates`),
        validate: (res, { candidates }) => res.status === 200 && !!candidates,
      })
        .then(({ candidates }) => {
          candidates.filter(Boolean).forEach((candidate) => {
            pc.current?.addIceCandidate(new RTCIceCandidate(candidate));
          });
          return null;
        })
        .catch(console.error);
    }
  }, [isPollOfferCandidates]);

  useEffect(() => {
    if (isPollAnswerCandidates) {
      poll<{ candidates: RTCIceCandidate[] }>({
        fn: () => fetch(`${config.API_URL}/rooms/${id}/answer-candidates`),
        validate: (res, { candidates }) => res.status === 200 && !!candidates,
      })
        .then(({ candidates }) => {
          candidates.filter(Boolean).forEach((candidate) => {
            pc.current?.addIceCandidate(new RTCIceCandidate(candidate));
          });
          return null;
        })
        .catch(console.error);
    }
  }, [isPollAnswerCandidates]);

  const createOffer = async () => {
    if (pc.current) {
      pc.current.onicecandidate = (event: RTCPeerConnectionIceEvent) => {
        if (event.candidate) {
          fetch(`${config.API_URL}/rooms/${id}/offer-candidates`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(event.candidate.toJSON()),
          });
        }
      };

      const offer = await pc.current.createOffer();
      await pc.current.setLocalDescription(offer);

      await fetch(`${config.API_URL}/rooms/${id}/offer`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(offer),
      });

      setIsPollAnswer(true);
      setIsPollAnswerCandidates(true);
    }
  };

  const createAnswer = async (offerDescription: RTCSessionDescriptionInit) => {
    if (pc.current) {
      pc.current.onicecandidate = (event) => {
        if (event.candidate) {
          fetch(`${config.API_URL}/rooms/${id}/answer-candidates`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(event.candidate.toJSON()),
          });
        }
      };

      await pc.current.setRemoteDescription(new RTCSessionDescription(offerDescription));

      const answer = await pc.current.createAnswer();
      await pc.current.setLocalDescription(answer);

      await fetch(`${config.API_URL}/rooms/${id}/answer`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(answer),
      });

      setIsPollOfferCandidates(true);
    }
  };

  const setLocalStream = (stream: MediaStream) => {
    stream.getTracks().forEach((track) => {
      pc.current?.addTrack(track, stream);
    });
  };

  const getRemoteStream = () => new Promise<MediaStream>(((resolve) => {
    if (pc.current) {
      pc.current.ontrack = (event) => {
        const [stream] = event.streams;
        resolve(stream);
      };
    }
  }));

  return (
    <PeerConnectionContext.Provider value={{
      createOffer,
      createAnswer,
      setLocalStream,
      getRemoteStream,
      hangup,
    }}
    >
      {children}
    </PeerConnectionContext.Provider>
  );
};

export const usePeerConnection = (): Context => {
  const context = useContext(PeerConnectionContext);
  if (!context) {
    throw new Error('usePeerConnection must be used within a PeerConnectionProvider');
  }

  return context;
};
