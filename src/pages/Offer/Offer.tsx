import React from 'react';

import { useHistory } from 'react-router-dom';

import './Offer.scss';
import config from '@/config';

const Offer = (): JSX.Element => {
  const history = useHistory();

  const handleStartConversation = async (): Promise<void> => {
    const { roomId: id }: { roomId: string } = await fetch(
      `${config.API_URL}/rooms`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      },
    ).then((res) => res.json());

    history.push(`/room/${id}`);
  };

  return (
    <div className="Offer">
      <div className="Offer__action-panel">
        <button
          type="button"
          onClick={handleStartConversation}
        >
          Start Conversation
        </button>
      </div>
    </div>
  );
};
export default Offer;
