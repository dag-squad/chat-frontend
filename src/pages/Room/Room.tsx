import React, { useEffect, useRef } from 'react';

import { useParams } from 'react-router-dom';

import './Room.scss';
import config from '@/config';
import { usePeerConnection } from '@contexts';

const Room = (): JSX.Element => {
  const { id } = useParams<{ id: string }>();

  const peerConnectionManager = usePeerConnection();
  const localVideoRef = useRef<HTMLVideoElement | null>(null);
  const remoteVideoRef = useRef<HTMLVideoElement | null>(null);
  const localStream = useRef<MediaStream | null>(null);
  const remoteStream = useRef<MediaStream | null>(null);

  const handleWebcam = async (): Promise<void> => {
    if (localStream.current) {
      localStream.current = await navigator.mediaDevices.getUserMedia({
        video: {
          frameRate: {
            exact: 30,
            ideal: 60,
          },
        },
        audio: {
          echoCancellation: true,
          noiseSuppression: true,
        },
      });

      peerConnectionManager.setLocalStream(localStream.current);
    }

    peerConnectionManager.getRemoteStream()
      .then((stream) => {
        stream.getTracks().forEach((track) => {
          if (remoteStream.current) {
            remoteStream.current.addTrack(track);
          }
        });
        return null;
      })
      .catch(console.error);

    if (localVideoRef.current && localStream.current) {
      localVideoRef.current.srcObject = localStream.current;
      localVideoRef.current.style.transform = 'scale(-1, 1)';
    }

    if (remoteVideoRef.current && remoteStream.current) {
      remoteVideoRef.current.srcObject = remoteStream.current;
      remoteVideoRef.current.style.transform = 'scale(-1, 1)';
    }
  };

  const handleCallOffer = async (): Promise<void> => {
    await peerConnectionManager.createOffer();
  };

  const handleCallAnswer = async (offerDescription: RTCSessionDescriptionInit): Promise<void> => {
    await peerConnectionManager.createAnswer(offerDescription);
  };

  useEffect(() => {
    localStream.current = new MediaStream();
    remoteStream.current = new MediaStream();
  }, []);

  useEffect(() => {
    handleWebcam()
      .then(() => fetch(`${config.API_URL}/rooms/${id}/offer`))
      .then((res) => res.json())
      .then((json: { status: string } | RTCSessionDescriptionInit) => (
        'status' in json ? handleCallOffer() : handleCallAnswer(json)
      ))
      .catch(console.error);
  }, []);

  return (
    <div className="Room">
      <div className="Room__video-container">
        <video
          className="Room__video-container-item"
          ref={localVideoRef}
          autoPlay
          playsInline
          width={460}
          height={345}
          muted
        />
        {/* eslint-disable-next-line jsx-a11y/media-has-caption */}
        <video
          className="Room__video-container-item"
          ref={remoteVideoRef}
          autoPlay
          playsInline
          width={460}
          height={345}
        />
      </div>
    </div>
  );
};

export default Room;
