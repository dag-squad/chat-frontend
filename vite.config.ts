import { defineConfig } from 'vite';
import reactRefresh from '@vitejs/plugin-react-refresh';
import eslintPlugin from 'vite-plugin-eslint';
import { readFileSync } from 'fs';
import { resolve } from 'path';
import { env } from 'process';

// https://vitejs.dev/config/
export default defineConfig(() => {
  const host = env.CHAT_APP_HOST ?? 'chat-app.local';

  return {
    server: {
      host,
      https: {
        key: readFileSync('./dev-certs/192.168.88.2+254-key.pem'),
        cert: readFileSync('./dev-certs/192.168.88.2+254.pem'),
      },
    },
    resolve: {
      alias: {
        '@': resolve(__dirname, './src'),
        '@contexts': resolve(__dirname, './src/contexts'),
        '@pages': resolve(__dirname, './src/pages'),
        '@utils': resolve(__dirname, './src/utils'),
      },
    },
    plugins: [
      reactRefresh(),
      eslintPlugin({
        throwOnWarning: false,
        cache: false,
      }),
    ],
  };
});
